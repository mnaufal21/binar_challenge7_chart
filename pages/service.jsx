import React from 'react'
import { Pie } from 'react-chartjs-2';
// eslint-disable-next-line
import { Chart } from "chart.js/auto";

const service = () => {
  const dataChart = {
    labels: ["Mercedes", "Mitsubishi", "Ferarri", "Honda", "Suzuki", "Wuling"],
    datasets: [
      {
        label: "banyak peminat",
        backgroundColor: ["blue", "green", "orange", "red", "yellow", "black"],
        data: [25, 50, 11, 15, 5, 5],
      },
    ]
  }
  return (
    <>
        <section className="our-services">
            <div className="chart">
                <Pie data={dataChart} />
            </div>
        </section>
    </>
  )
}

export default service