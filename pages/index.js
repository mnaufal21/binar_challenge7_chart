import Head from 'next/head'
// eslint-disable-next-line
import styles from '../styles/Home.module.css'
import Header from '../pages/header'
import Service from './service';


export default function Home() {
  return (
    <div>
      <Head>
        <title>Binar Car Rental</title>
        <meta name="description" content="Binar Car Rental - Landing Page" />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous"
        />
      </Head>

      <main>
        <Header />
        <Service />
      </main>

      <footer>
        <script 
          src="https://unpkg.com/react/umd/react.production.min.js" 
          crossorigin></script>

        <script
          src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"
          crossorigin></script>

        <script
          src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"
          crossorigin></script> 
      </footer>
    </div>
  )
}
