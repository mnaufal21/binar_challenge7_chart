import React from 'react';
import { Navbar, Nav, Button } from 'react-bootstrap';
import Link from "next/link";



const Header = () => {
  return (
    <>
      <section className="header">
        <Navbar className="navbar">
          <Navbar.Brand href="#" className="logo">
            <img src="/images/logo.svg" alt="Logo Binar Car Rental" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="navigation"
              navbarScroll
            >
              <Nav.Link href="#action1" className="service">Our Services</Nav.Link>
              <Nav.Link href="#action2" className="why">Why Us</Nav.Link>
              <Nav.Link href="#action3" className="testimon">Testimonial</Nav.Link>
              <Nav.Link href="#action4" className="faq">FAQ</Nav.Link>
              <Button variant="outline-success" className="regis">Register</Button>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <h1 className="sale">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
        <p className="come">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
        <Button variant="outline-success" className="begin">
          <Link href="/car">Mulai Sewa Mobil</Link>
        </Button>
        <img src="/images/img_car.png" alt="Mobil" className="car" />
      </section>
      
    </>
  )
}

export default Header